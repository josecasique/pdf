json.extract! entrega, :id, :ticket, :asegurado, :medico, :hospital, :coaseguro, :icd, :ctp, :ctp2, :folio, :reclamacion, :ingreso, :solicitud, :fecha_even, :created_at, :updated_at
json.url entrega_url(entrega, format: :json)
