require "application_system_test_case"

class EntregasTest < ApplicationSystemTestCase
  setup do
    @entrega = entregas(:one)
  end

  test "visiting the index" do
    visit entregas_url
    assert_selector "h1", text: "Entregas"
  end

  test "creating a Entrega" do
    visit entregas_url
    click_on "New Entrega"

    fill_in "Asegurado", with: @entrega.asegurado
    fill_in "Coaseguro", with: @entrega.coaseguro
    fill_in "Ctp", with: @entrega.ctp
    fill_in "Ctp2", with: @entrega.ctp2
    fill_in "Fecha even", with: @entrega.fecha_even
    fill_in "Folio", with: @entrega.folio
    fill_in "Hospital", with: @entrega.hospital
    fill_in "Icd", with: @entrega.icd
    fill_in "Ingreso", with: @entrega.ingreso
    fill_in "Medico", with: @entrega.medico
    fill_in "Reclamacion", with: @entrega.reclamacion
    fill_in "Solicitud", with: @entrega.solicitud
    fill_in "Ticket", with: @entrega.ticket
    click_on "Create Entrega"

    assert_text "Entrega was successfully created"
    click_on "Back"
  end

  test "updating a Entrega" do
    visit entregas_url
    click_on "Edit", match: :first

    fill_in "Asegurado", with: @entrega.asegurado
    fill_in "Coaseguro", with: @entrega.coaseguro
    fill_in "Ctp", with: @entrega.ctp
    fill_in "Ctp2", with: @entrega.ctp2
    fill_in "Fecha even", with: @entrega.fecha_even
    fill_in "Folio", with: @entrega.folio
    fill_in "Hospital", with: @entrega.hospital
    fill_in "Icd", with: @entrega.icd
    fill_in "Ingreso", with: @entrega.ingreso
    fill_in "Medico", with: @entrega.medico
    fill_in "Reclamacion", with: @entrega.reclamacion
    fill_in "Solicitud", with: @entrega.solicitud
    fill_in "Ticket", with: @entrega.ticket
    click_on "Update Entrega"

    assert_text "Entrega was successfully updated"
    click_on "Back"
  end

  test "destroying a Entrega" do
    visit entregas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Entrega was successfully destroyed"
  end
end
