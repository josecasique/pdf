Rails.application.routes.draw do
  resources :entregas
  resources :products
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
