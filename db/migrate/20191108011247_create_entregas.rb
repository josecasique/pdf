class CreateEntregas < ActiveRecord::Migration[6.0]
  def change
    create_table :entregas do |t|
      t.string :ticket
      t.string :asegurado
      t.string :medico
      t.string :hospital
      t.string :coaseguro
      t.integer :icd
      t.integer :ctp
      t.integer :ctp2
      t.integer :folio
      t.string :reclamacion
      t.string :ingreso
      t.string :solicitud
      t.string :fecha_even

      t.timestamps
    end
  end
end
