# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Product.create(title:'Aguacate', pricing:10)


Entrega.create(ticket: 'CPO07190001',
 			   asegurado: 'Andre Ivan Cachola',
 			   medico: 'Barrera Lozano Adrian',
 			   hospital: 'Doctors hospital',
 			   coaseguro: '10%',
 			   icd: 3333,
 			   ctp: 11001,
 			   ctp2: 11443,
 			   folio: 001,
 			   reclamacion: '2019HI000025',
 			   ingreso: '2019-11-12',
 			   solicitud: 'CPO07190001',
 			   fecha_even: '13/112019')

