# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_08_011247) do

  create_table "entregas", force: :cascade do |t|
    t.string "ticket"
    t.string "asegurado"
    t.string "medico"
    t.string "hospital"
    t.string "coaseguro"
    t.integer "icd"
    t.integer "ctp"
    t.integer "ctp2"
    t.integer "folio"
    t.string "reclamacion"
    t.string "ingreso"
    t.string "solicitud"
    t.string "fecha_even"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "title"
    t.integer "pricing"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
